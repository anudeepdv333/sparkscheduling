import java.lang.management.ManagementFactory
import sys.process._
object wordcount{
  def main(args: Array[String]) {

val startTimeMillis = System.currentTimeMillis()

/* your code goes here */
val t1 = System.nanoTime
val textFile = sc.textFile("/home/anudeep/Desktop/ip2.txt")
val counts = textFile.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey(_ + _)
counts.saveAsTextFile("/home/anudeep/Desktop/output23")


val endTimeMillis = System.currentTimeMillis()
val durationSeconds = (endTimeMillis - startTimeMillis) / 1000

println(durationSeconds)

}
}

