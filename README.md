# Apache Spark

Spark is a fast and general cluster computing system for Big Data. It provides
high-level APIs in Scala, Java, Python, and R, and an optimized engine that
supports general computation graphs for data analysis. It also supports a
rich set of higher-level tools including Spark SQL for SQL and DataFrames,
MLlib for machine learning, GraphX for graph processing,
and Spark Streaming for stream processing.

<http://spark.apache.org/>


## Building Spark

    ./build/sbt package
(You do not need to do this if you downloaded a pre-built package.)

You can build Spark using more than one thread by using the -T option with Maven, see ["Parallel builds in Maven 3"](https://cwiki.apache.org/confluence/display/MAVEN/Parallel+builds+in+Maven+3).
More detailed documentation is available from the project site, at
["Building Spark"](http://spark.apache.org/docs/latest/building-spark.html).

For general development tips, including info on developing Spark using an IDE, see ["Useful Developer Tools"](http://spark.apache.org/developer-tools.html).

## Interactive Scala Shell

The easiest way to start using Spark is through the Scala shell:

    ./bin/spark-shell

## EXPERIMENTAL RESULTS


We had multiple results for each of the methods we made use of to
optimize the default Spark Scheduler. All the results obtained were
clearly dependent not only on the cores which they run but also on
various other parameters like the data dependencies between each
task’s data and size of the data.

1.) Random Core Selection: This method didn’t prove to be much
effective and produced results that were comparatively worse than the
default Spark’s Scheduler.

2.) Round Robin Algorithm: This method also was a failure as it didn’t
produce any desired results and almost took the same time as the
original Spark Scheduler for small input data sizes, while took
noticeably more time for larger data sets compared to the default
scheduler.

3.) Least CPU Used: This resulted in certain positive results for certain
kind of data usually in smaller sizes like around 1.5GB files, and it is in
this approach where we realized that Data Dependency exists between
the executing cores in the CPU. But Unfortunately it didn’t workout
for large data files around 5GB for which the time was almost the same
as the time taken by the Default Spark Scheduler to execute.

4.) Full Stage Execution: Here we implemented two scheduling
methods. The first method where we ran all threads of a stage to one
single core gave bad results as the threads of a stage were made to wait
to run on a single core. The other method where we ran each core of a
stage into a corresponding available(free) core gave much better
results and can be seen 

